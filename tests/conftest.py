import pytest


@pytest.fixture(scope='function', autouse=True)
def print_name(request):
    """
    executed automatically for every test
    does not have to be called
    prints the name of the test, as well as the name and path of the file the test is in
    """
    name_of_test = request.node.name
    path_and_file = request.node.location[0]
    print(f"\n==================   start of '{name_of_test}' in file '{path_and_file}'      ================== ")
    yield  # test is executed
    print(f"\n================================================================================================ ")
