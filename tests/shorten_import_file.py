import json
from datetime import datetime
import random
from repository.in_data_handling import get_ads

number_of_ads = 1000

"""
Set the desired number of ads
this number of ads will be picked randomly 
a file with the selected ads will be written, use that for import
"""


def select_random_ads(all_ads, max_number):
    if max_number > len(all_ads):
        max_number = len(all_ads)
    return random.sample(all_ads, max_number)


def write_to_file(ads_to_write, file_name):
    file_name = f"output_{datetime.now().strftime('%Y_%m_%d_%H.%M')}_ads_{len(ads_to_write)}_out.json"

    with open(file_name, 'w', encoding='utf-8') as f:
        for ad in ads_to_write:
            json.dump(ad, f)
            f.write("\n")
    print(f"{len(ads_to_write)} ads written to file '{file_name}'")
    return file_name


if __name__ == '__main__':
    all_ads, file_name_with_timestamp = get_ads()
    ads_to_write = select_random_ads(all_ads, max_number=number_of_ads)
    short_file_name = write_to_file(ads_to_write, file_name_with_timestamp)
