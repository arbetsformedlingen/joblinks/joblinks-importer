import json
import sys
import glob
from datetime import datetime
import ndjson
from repository.path_util import root_dir
import settings
from loguru_wrapper import log


def _open_the_file():
    data = []
    file_name = get_filename_by_pattern()
    log.info(f"File to be loaded: {file_name}")
    with open(file_name, 'r', encoding='utf-8') as data_file:
        for item in data_file.readlines():
            data.append(json.loads(item))
    log.info(f'Loaded {len(data)} ads from {file_name}')
    return data, file_name


def get_ads():
    # If ads should be read from file...
    if settings.JOBLINKS_READ_FROM_FILE:
        ads, filename = _open_the_file()
        timestamp = get_timestamp_from_filename(filename)
    # If ads should be read from stdin (i.e. default)...
    else:
        ads = ndjson.load(sys.stdin)
        timestamp = f"stdin-{settings.JOBLINKS_STDIN_NAME}" if settings.JOBLINKS_STDIN_NAME else "stdin"
        log.debug(f'Done with stdin read. Total ads: {len(ads)}, timestamp: {timestamp}')
        if not len(ads):
            log.error("Failed to retrieve any ads. Exit!")
            sys.exit(1)
    return ads, timestamp


def get_filename_by_pattern(path_and_pattern=None):
    """
    finds all files in a directory matching a specific pattern.
    Sorts the list of files alphabetically in reverse order to be able to pick the latest file (based on name)
    expects format like output_2020-02-27-05-38-54.json  where months and days must be two characters
    """
    if not path_and_pattern:
        path_and_pattern = get_path_and_pattern()
    files = glob.glob(path_and_pattern)
    files.sort(reverse=True)  # latest first
    if files:
        return files[0]
    else:
        log.error(f"no file matching pattern {path_and_pattern} was found. Exit!")
        sys.exit(-1)


def get_path_and_pattern(dir_for_import_files=None):
    if not dir_for_import_files:
        dir_for_import_files = root_dir / settings.JOBLINKS_FOLDER
    full_path_and_pattern = dir_for_import_files.joinpath(settings.JOBLINKS_FILE_PATTERN)
    return str(full_path_and_pattern)


def get_timestamp_from_filename(path_and_filename):
    """
    takes the string between 'output_' and the last dot
    'output_2020-11-27-05-39-54.json' ->  '2020-11-27-05-39-54' ->
    """
    if 'output_' in path_and_filename:
        tmp = path_and_filename.split('output_')[-1].lower()
        return f"file_{tmp.split('.json')[0]}"
    else:
        return datetime.now().strftime('%Y%m%d-%H.%M')


def check_number_of_valid_ads(ads, ads_number_current_index, source_name):
    """
    check that the number of ads to be imported with 'isValid' attribute True
    is at least a percentage of existing number of ads defined by settings.NEW_ADS_COEF
    """
    number_of_valid_ads = 0
    for ad in ads:
        if ad['isValid']:
            number_of_valid_ads += 1
    percent = (number_of_valid_ads / ads_number_current_index) * 100 if ads_number_current_index else 100
    log.info(
        f"Valid: {number_of_valid_ads} of: {len(ads)} ads in source: {source_name}, it is: {percent:.1f}% of ads in current index")
    if number_of_valid_ads < ads_number_current_index * settings.NEW_ADS_COEF:
        log.error(f"Too FEW ads in import. New: {number_of_valid_ads}, Current: {ads_number_current_index}, coefficient: {settings.NEW_ADS_COEF}")

        return False
    else:
        log.info(f'OK number of ads in latest import.  New: {number_of_valid_ads}, Current: {ads_number_current_index}, source: {source_name}')
        return True
