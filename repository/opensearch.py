import sys
import time
from opensearchpy import OpenSearch
from opensearchpy.helpers import bulk
import settings
from datetime import datetime
from loguru_wrapper import log

auth = None
if settings.ES_USER and settings.ES_PWD:
    auth = (settings.ES_USER, settings.ES_PWD)

if auth:
    es = OpenSearch(hosts=[{'host': host, 'port': settings.ES_PORT} for host in settings.ES_HOST],
                    http_compress=True,  # enables gzip compression for request bodies
                    http_auth=auth,
                    use_ssl=settings.ES_USE_SSL,
                    verify_certs=settings.ES_VERIFY_CERTS,
                    ssl_assert_hostname=False,
                    ssl_show_warn=False,
                    timeout=settings.ES_TIMEOUT,
                    max_retries=10,
                    retry_on_timeout=True,
                    )
else:
    es = OpenSearch(hosts=[{'host': host, 'port': settings.ES_PORT} for host in settings.ES_HOST],
                    http_compress=True,  # enables gzip compression for request bodies
                    use_ssl=settings.ES_USE_SSL,
                    verify_certs=settings.ES_VERIFY_CERTS,
                    ssl_assert_hostname=False,
                    ssl_show_warn=False,
                    timeout=settings.ES_TIMEOUT,
                    max_retries=10,
                    retry_on_timeout=True,
                    )

log.info(f"Opensearch object is set using host: {', '.join(settings.ES_HOST)} (port {settings.ES_PORT}), user: {settings.ES_USER}")

def _bulk_generator(documents, indexname, idkey):
    log.debug(f"(_bulk_generator) index: {indexname}, idkey: {idkey}")
    for document in documents:
        doc_id = '-'.join([document[key] for key in idkey]) \
            if isinstance(idkey, list) else document[idkey]

        yield {
            '_index': indexname,
            '_id': doc_id,
            '_source': document
        }


def bulk_index(documents, indexname, idkey='id'):
    action_iterator = _bulk_generator(documents, indexname, idkey)

    result = bulk(es, action_iterator, request_timeout=settings.ES_TIMEOUT, raise_on_error=True, yield_ok=False)
    log.info(f"(bulk_index) result: {result[0]}")
    return result[0]


def verify_import(es_index, len_imported_ads):
    number_of_ads_in_opensearch = document_count(es_index)
    assert isinstance(number_of_ads_in_opensearch, str), (
        f"Expected a value as str but it was of type {type(number_of_ads_in_opensearch)}. Value: {number_of_ads_in_opensearch}")
    assert int(number_of_ads_in_opensearch) == len_imported_ads, f"opensearch: {number_of_ads_in_opensearch}"
    log.info(f"Verified: {number_of_ads_in_opensearch} ads in: {es_index}")


def document_count(es_index: str) -> str:
    # Returns the number of documents in the index or 0 if operation fails.
    try:
        es.indices.refresh(index=es_index)
        num_doc_opensearch = es.cat.count(index=es_index, params={"format": "json"}, request_timeout=settings.ES_TIMEOUT)[0]['count']
        log.info(f'Docs count from _cat api: {num_doc_opensearch}, index: {es_index}')
    except Exception as e:
        log.error(f"Operation failed when trying to count ads in index {es_index}: {e}")
        num_doc_opensearch = "0"
    return num_doc_opensearch


def index_exists(indexname):
    es_available = False
    fail_count = 0
    while not es_available:
        try:
            result = es.indices.exists(index=[indexname])
            es_available = True
            return result
        except Exception as e:
            if fail_count > 1:
                # Opensearch has its own failure management, so > 1 is enough.
                log.error(f"Opensearch not available after try: {fail_count}. Stop trying. {e}")
                raise e
            fail_count += 1
            log.warning(f"Connection fail: {fail_count} for index: {indexname} with {e}")
            time.sleep(1)


def alias_exists(aliasname):
    return es.indices.exists_alias(name=[aliasname])


def get_alias(aliasname):
    return es.indices.get_alias(name=[aliasname])


def put_alias(indexlist, aliasname):
    return es.indices.put_alias(index=indexlist, name=aliasname)


def setup_index(file_timestamp, es_alias, es_mapping):
    if len(sys.argv) > 1:
        es_alias = sys.argv[1]

    timestamp = datetime.now().strftime('%Y%m%d-%H.%M')
    es_index = format_index_name(es_alias, file_timestamp, timestamp)

    create_index(es_index, es_mapping)
    return es_index


def create_index(indexname, extra_mappings=None):
    if index_exists(indexname):
        log.info(f"Index {indexname} already exists ")
        return

    log.info(f"Creating index: {indexname}")

    basic_body = {
        "mappings": {
            "properties": {
                "timestamp": {
                    "type": "long"
                },
            }
        }
    }

    if extra_mappings:
        body = extra_mappings
        if 'mappings' in body:
            body.get('mappings', {}).get('properties', {})['timestamp'] = {'type': 'long'}
        else:
            body.update(basic_body)
    else:
        body = basic_body

    result = es.indices.create(index=indexname, body=body, wait_for_active_shards=1)
    if 'error' in result:
        log.error(f"Error: {result} on create index: {indexname} , mapping: {body}")
    else:
        log.info(f"New index created: {indexname} , mapping: {body}")


def add_indices_to_alias(indexlist, aliasname):
    actions = {
        "actions": [
            {"add": {"indices": indexlist, "alias": aliasname}}
        ]
    }
    response = es.indices.update_aliases(body=actions)
    log.info(f"Added: {actions}")
    return response


def update_alias(indexnames, old_indexlist, aliasname):
    actions = {
        "actions": []
    }
    for index in old_indexlist:
        actions["actions"].append({"remove": {"index": index, "alias": aliasname}})

    actions["actions"].append({"add": {"indices": indexnames, "alias": aliasname}})
    es.indices.update_aliases(body=actions)
    log.info(f"Updated: {actions}")


def format_index_name(es_alias, file_timestamp, now_timestamp):
    es_index_name = f"{es_alias}-{file_timestamp}-{now_timestamp}"
    return es_index_name
