import sys
from repository import opensearch
from valuestore.taxonomy import find_concept_by_legacy_ams_taxonomy_id as fcbla
from opensearchpy import RequestError
from loguru_wrapper import log


tax_value_cache = {}


def get_concept_by_legacy_id(taxtype, legacy_id, not_found_response=None):
    key = "concept-%s-%s-%s" % (str(taxtype), legacy_id, str(not_found_response))
    if key not in tax_value_cache:
        try:
            value = fcbla(opensearch.es, taxtype, legacy_id, not_found_response)
        except RequestError:
            log.warning(f'Taxonomy RequestError for request with arguments type: {taxtype} and id: {legacy_id}')
            value = not_found_response
            log.info(f"(get_concept_by_legacy_id) set value: {value}")
        if value:
            tax_value_cache[key] = value
        else:
            tax_value_cache[key] = {}
            log.warning(f"(get_concept_by_legacy_id) set empty to tax_value_cache[key]: [{key}]")
    cached = dict(tax_value_cache.get(key, {}))
    log.debug(f"(get_concept_by_legacy_id) returns cached: {cached}")
    return cached
