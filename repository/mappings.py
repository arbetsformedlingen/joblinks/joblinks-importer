joblinks_mappings = {
    "settings": {
        "analysis": {
            "analyzer": {
                "simple_word_splitter": {
                    "type": "custom",
                    "tokenizer": "whitespace",
                    "filter": ["lowercase"],
                    "char_filter": ["punctuation_filter"]
                },
                "wildcard_prefix": {
                    "type": "custom",
                    "tokenizer": "whitespace",
                    "filter": ["lowercase", "edgengram_filter"],
                    "char_filter": ["punctuation_filter"]
                },
                "wildcard_suffix": {
                    "type": "custom",
                    "tokenizer": "whitespace",
                    "filter": ["lowercase", "reverse", "edgengram_filter", "reverse"],
                    "char_filter": ["punctuation_filter"]
                },
                "trigram": {
                    "type": "custom",
                    "tokenizer": "standard",
                    "filter": ["lowercase", "shingle", "swedish_stop", "swedish_keywords"]
                },
                "reverse": {
                    "type": "custom",
                    "tokenizer": "standard",
                    "filter": ["lowercase", "reverse", "swedish_stop", "swedish_keywords"]
                }
            },
            "filter": {
                "edgengram_filter": {
                    "type": "edge_ngram",
                    "min_gram": 3,
                    "max_gram": 30
                },
                "shingle": {
                    "type": "shingle",
                    "min_shingle_size": 2,
                    "max_shingle_size": 3
                },
                "swedish_stop": {
                    "type": "stop",
                    "stopwords": "_swedish_"
                },
                "swedish_keywords": {
                    "type": "keyword_marker",
                    "keywords": ["exempel"]
                }
            },
            "char_filter": {
                "punctuation_filter": {
                    "type": "mapping",
                    "mappings": [
                        ". => ",
                        ", => \\u0020",
                        "\\u00A0 => \\u0020",
                        ": => ",
                        "! => "
                    ]
                }
            }
        }
    },
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            },
            "hashsum": {
                "type": "text",
            },
            "date_to_display_as_publish_date": {
                "type": "date"
            },
            "source_links": {
                "type": "nested",
                "properties": {
                    "label": {
                        "type": "keyword"
                    },
                    "url": {
                        "type": "keyword"
                    }
                }
            },
            "originalJobPosting": {
                "properties": {
                    "title": {
                        "type": "text",
                        "fields": {
                            "words": {
                                "type": "text",
                                "analyzer": "simple_word_splitter"
                            },
                            "prefix": {
                                "type": "text",
                                "analyzer": "wildcard_prefix",
                                "search_analyzer": "simple_word_splitter"
                            },
                            "suffix": {
                                "type": "text",
                                "analyzer": "wildcard_suffix",
                                "search_analyzer": "simple_word_splitter"
                            },
                        }
                    }
                }
            },
            "workplace_addresses": {
                "type": "nested",
                "properties": {
                    "municipality": {
                        "type": "keyword",
                    },
                    "municipality_concept_id": {
                        "type": "keyword",
                    },
                    "region": {
                        "type": "keyword",
                    },
                    "region_concept_id": {
                        "type": "keyword",
                    },
                    "country": {
                        "type": "keyword",
                        "null_value": "Sverige"
                    },
                    "country_concept_id": {
                        "type": "keyword",
                        "null_value": "i46j_HmG_v64"
                    },
                },
            },
            "occupation_field": {
                "properties": {
                    "concept_id": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "label": {
                        "type": "text",
                    },
                }
            },
            "occupation_group": {
                "properties": {
                    "concept_id": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "label": {
                        "type": "text",
                    },
                }
            },
            "keywords": {
                "type": "object",
                "properties": {
                    "enriched": {
                        "type": "object",
                        "properties": {
                            "occupation": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "skill": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "trait": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "location": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    },
                    "enriched_typeahead_terms": {
                        "type": "object",
                        "properties": {
                            "occupation": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "skill": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "trait": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "location": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    },
                    "extracted": {
                        "type": "object",
                        "properties": {
                            "occupation": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "skill": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "location": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            },
                            "employer": {
                                "type": "text",
                                "fields": {
                                    "raw": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    }
                }
            },
        }
    }
}
