import re
from repository.taxonomy import get_concept_by_legacy_id
from loguru_wrapper import log


KEYWORDS_ENRICHED_FIELD = 'enriched'
KEYWORDS_ENRICHED_TYPEDAHEAD_TERMS_FIELD = 'enriched_typeahead_terms'
KEYWORDS_EXTRACTED_FIELD = 'extracted'

SOURCE_KEYWORDS_ENRICHED_FIELD = 'concept_label'
SOURCE_KEYWORDS_ENRICHED_TYPEDAHEAD_TERMS_FIELD = 'term'

SOURCE_TYPE_OCCUPATION = "occupations"
SOURCE_TYPE_SKILL = "competencies"
SOURCE_TYPE_TRAIT = "traits"
SOURCE_TYPE_LOCATION = "geos"

TARGET_TYPE_OCCUPATION = "occupation"
TARGET_TYPE_SKILL = "skill"
TARGET_TYPE_TRAIT = "trait"
TARGET_TYPE_LOCATION = "location"
TARGET_TYPE_COMPOUND = "compound"


def get_null_safe_value(element, key, replacement_val):
    val = element.get(key, replacement_val)
    if val is None:
        val = replacement_val
    return val


def convert_batch(ad_batch):
    converted_ads_batch = []
    for ad in ad_batch:
        if ad.get('isValid', None):
            try:
                converted_ads_batch.append(convert_ad(ad))
            except Exception as e:
                # ignore error. Log and continue with next ad
                scraper = ad.get('originalJobPosting', {}).get('scraper', 'unknown scraper')
                ad_id = ad.get('id', 'no id found')
                log.error(f"Ad {ad_id} from scraper: {scraper} is not imported because of {e.__class__.__name__}:  {e}")
                pass

    log.info(f"Converted: {len(converted_ads_batch)} ads to proper format ...")
    return converted_ads_batch


def convert_ad(ad):
    converted_ad = dict()
    converted_ad['id'] = ad.get('id', '')

    date_from_ad = ad.get('date_to_display_as_publish_date', None)
    converted_ad['date_to_display_as_publish_date'] = date_from_ad
    converted_ad['detected_language'] = ad.get('detected_language', '')

    # If workplace addresses come in section with multiple addresses...
    if ad.get('workplace_addresses'):
        converted_ad['workplace_addresses'] = ad.get('workplace_addresses')

    # If workplace address comes in legacy format with municipality and region...
    # TODO remove.
    elif ad.get('municipality_concept_id') or ad.get('region_concept_id'):
        converted_ad['workplace_addresses'] = [{
            'municipality_concept_id': ad.get('municipality_concept_id', ''),
            'municipality': '',
            'region_concept_id': ad.get('region_concept_id', ''),
            'region': '',
            'country_concept_id': ad.get('country_concept_id', ''),
            'country': '',
        }]

    occupation_info = get_concept_by_legacy_id('group', ad.get('ssyk_lvl4', ''))
    occupation_group = None
    occupation_group_concept_id = None
    occupation_field = None
    occupation_field_concept_id = None

    if occupation_info:
        occupation_group = occupation_info.get('label', '')
        occupation_group_concept_id = occupation_info.get('concept_id', '')
        occupation_field_info = occupation_info.get('parent', {})
        if occupation_field_info:
            occupation_field = occupation_field_info.get('label', '')
            occupation_field_concept_id = occupation_field_info.get('concept_id', '')

    converted_ad['occupation_group'] = {
        'label': occupation_group,
        'concept_id': occupation_group_concept_id
    }

    converted_ad['occupation_field'] = {
        'label': occupation_field,
        'concept_id': occupation_field_concept_id
    }

    converted_ad['hashsum'] = ad.get('hashsum', '')
    converted_ad['originalJobPosting'] = {}
    original_job_post = ad.get('originalJobPosting', '')
    if original_job_post:
        converted_ad['originalJobPosting'] = {
            'title': original_job_post.get('title', ''),
            'brief': ad.get('brief_description', ''),
            'sameAs': original_job_post.get('sameAs', ''),
            'employer': {'name': original_job_post.get('hiringOrganization', {}).get('name', '')}
        }

    enriched_keywords = ad.get('text_enrichments_results', {}).get('enrichedbinary_result', {}) \
        .get('enriched_candidates', {})
    converted_ad['keywords'] = {}
    converted_ad['keywords']['enriched'] = {}
    converted_ad['keywords']['enriched_typeahead_terms'] = {}
    if enriched_keywords:
        converted_ad['keywords'][KEYWORDS_ENRICHED_FIELD] = process_enriched_candidates(
            enriched_keywords, SOURCE_KEYWORDS_ENRICHED_FIELD)
        converted_ad['keywords'][KEYWORDS_ENRICHED_TYPEDAHEAD_TERMS_FIELD] = process_enriched_candidates(
            enriched_keywords, SOURCE_KEYWORDS_ENRICHED_TYPEDAHEAD_TERMS_FIELD)

    converted_ad['source_links'] = []
    source_links = ad.get('sourceLinks', '')
    if source_links:
        for source_link in source_links:
            converted_ad['source_links'].append({
                'label': source_link.get('displayName', None),
                'url': source_link.get('link', None),
            })
    return _add_keywords(converted_ad)


def process_enriched_candidates(enriched_candidates, original_field):
    keywords_enriched = {}
    keywords_enriched[TARGET_TYPE_OCCUPATION] = [candidate[original_field].lower()
                                                 for candidate in enriched_candidates[SOURCE_TYPE_OCCUPATION]]

    keywords_enriched[TARGET_TYPE_SKILL] = [candidate[original_field].lower()
                                            for candidate in enriched_candidates[SOURCE_TYPE_SKILL]]

    keywords_enriched[TARGET_TYPE_TRAIT] = [candidate[original_field].lower()
                                            for candidate in enriched_candidates[SOURCE_TYPE_TRAIT]]

    keywords_enriched[TARGET_TYPE_LOCATION] = [candidate[original_field].lower()
                                               for candidate in enriched_candidates[SOURCE_TYPE_LOCATION]]
    return keywords_enriched


def _add_keywords(annons):
    annons['keywords'].setdefault('extracted', {})
    for key_dict in [
        {
            'occupation': [
                'occupation_group.label',
                'occupation_field.label',
            ]
        },
        {
            'skill': [
            ]
        },
        {
            'location': [
                'workplace_address.municipality',
                'workplace_address.region',
                'workplace_address.country',
            ]
        },
        {
            'employer': [
                'originalJobPosting.employer.name',
            ]
        }
    ]:
        field = list(key_dict.keys())[0]
        keywords = []
        values = []
        for key in list(key_dict.values())[0]:
            values += _get_nested_value(key, annons)
        if field == 'employer':
            for value in _create_employer_name_keywords(values):
                keywords.append(value)
        elif field == 'location':
            for value in values:
                trimmed = _trim_location(value)
                if trimmed:
                    keywords.append(trimmed)
        else:
            for value in values:
                for kw in _extract_taxonomy_label(value):
                    keywords.append(kw)
        annons['keywords']['extracted'][field] = keywords
    return annons


def _get_nested_value(path, data):
    keypath = path.split('.')
    values = []
    for i in range(len(keypath)):
        element = data.get(keypath[i])
        if isinstance(element, str):
            values.append(element)
            break
        if isinstance(element, list):
            for item in element:
                if item:
                    values.append(item.get(keypath[i + 1]))
            break
        if isinstance(element, dict):
            data = element
    return values


def _create_employer_name_keywords(companynames):
    names = []
    for companyname in companynames or []:
        converted_name = companyname.lower().strip()
        converted_name = __rightreplace(converted_name, ' ab', '')
        converted_name = __leftreplace(converted_name, 'ab ', '')
        names.append(converted_name)

    if names:
        names.sort(key=lambda s: len(s))
        shortest = len(names[0])
        uniques = [names[0]]
        for i in range(1, len(names)):
            if names[i][0:shortest] != names[0] and names[i]:
                uniques.append(names[i])

        return uniques

    return []


def __rightreplace(astring, pattern, sub):
    return sub.join(astring.rsplit(pattern, 1))


def __leftreplace(astring, pattern, sub):
    return sub.join(astring.split(pattern, 1))


def _extract_taxonomy_label(label):
    if not label:
        return []
    try:
        label = label.replace('m.fl.', '').strip()
        if '-' in label:
            return [word.lower() for word in re.split(r'/', label)]
        else:
            return [word.lower().strip() for word in re.split(r'/|, | och ', label)]
    except AttributeError:
        log.warning('(extract_taxonomy_label) extract fail for: %s' % label)
    return []


def _trim_location(locationstring):
    # Look for unwanted words (see tests/unit/test_converter.py)
    pattern = '[0-9\\-]+|.+,|([\\d\\w]+\\-[\\d]+)|\\(.*|.*\\)|\\(\\)|\\w*\\d+\\w*'
    regex = re.compile(pattern)
    stopwords = ['box']
    if locationstring:
        # Magic regex
        valid_words = []
        for word in locationstring.lower().split():
            if word and not re.match(regex, word) and word not in stopwords:
                valid_words.append(word)
        return ' '.join(valid_words)
    return locationstring
