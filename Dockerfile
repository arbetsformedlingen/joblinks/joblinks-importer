FROM docker.io/library/python:3.11.10-slim-bookworm AS base

ENV TZ=Europe/Stockholm \
    LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    ES_HOST=https://3d1803df463441cfb7759de0d6573943.eu-west-1.aws.found.io \
    ES_PORT=9243 \
    ES_USER=joblinks
    # JOBLINKS_READ_FROM_FILE=false : default
    # ENV JOBLINKS_READ_FROM_FILE=true

# Install packages to allow apt to use a repository over HTTPS:
RUN apt-get update -y && \
    apt-get install -y git && \
    apt-get clean -y && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app

WORKDIR /app

RUN python -m pip install poetry \
    && python -m poetry config virtualenvs.create false \
    && python -m poetry install &&\
# Remove git because of current vulnerability https://access.redhat.com/security/cve/CVE-2023-27536
    apt-get remove -y git && \
    apt-get autoremove -y


###############################################################################
FROM base AS test

# runs unit tests with @pytest.mark.unit annotation only
RUN python -m poetry run pytest -m unit tests/unit_tests &&\
    touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

ENTRYPOINT [ "bash", "pipeline.sh" ]
