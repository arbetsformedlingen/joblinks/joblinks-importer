
### Fetch data files by downloading using elastic dump from existing elastic/opensearch:
Data files and mappings can be fetched by dumping from existing elastic/opensearch environment into files using elasticdump.
- In a command prompt: `npm install elasticdump -g`

- Download taxonomy index mapping. In a command prompt (change myusername, mypassword and elastic-url:port to existing environment):
```
  elasticdump --input=https://myusername:mypassword@elastic-url:port/taxonomy --output=[path localhost]\taxonomy_mapping.json --type=mapping
```
- Download taxonomy index data. In a command prompt:
```
  elasticdump --input=https://myusername:mypassword@elastic-url:port/taxonomy --output=[path localhost]:taxonomy_data.json --type=data --limit=1000
```

- Download taxonomy analyzer. In a command prompt:
```
  elasticdump --input=https://myusername:mypassword@elastic-url:port/taxonomy --output=[path localhost]:taxonomy_analyzer.json --type=analyzer --limit=1000
```

### Create opensearch indices from the datafiles using elastic dump:
When the files are downloaded (i.e. previous steps in this section), the indices and mappings can be created.

- Upload taxonomy mapping. In a command prompt:
```
  elasticdump --input=[file dir]\taxonomy_mapping.json --output=http://localhost:9200/taxonomy --type=mapping
```
- Upload taxonomy data. In a command prompt:
```
  elasticdump --input=[file dir]\taxonomy_data.json --output=http://localhost:9200/taxonomy --type=data --limit=1000
```
- Upload taxonomy analyzer. In a command prompt:
```
  elasticdump --input=[file dir]\taxonomy_analyzer.json --output=http://localhost:9200/taxonomy --type=analyzer
```
