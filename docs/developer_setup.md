# General Python guidelines
https://gitlab.com/arbetsformedlingen/documentation/-/blob/main/engineering/plattform/python-projects.md


# Install and prepare local opensearch:
Ignore and jump to next section if only importing to remote opensearch.

## Run downloaded Opensearch 
Download package with binaries from https://opensearch.org/downloads.html and run it.


## Run Opensearch in podman
This installs latest opensearch image with no authentication, e.g. username/password is not needed.
Prerequisite: Install podman + podman-compose.

### 1. Build a new dashboard
This disables the security plugin and builds a new image
tagged opensearch-dashboards-no-security.

- In a shell move to [this repo]/docker/
- Run `podman build --tag=opensearch-dashboards-no-security .`
`podman` can be substituted with `docker` if you prefer that tool.

### 2. Run opensearch:
This uses the new dashboard image previously built.
Starts the containers in detached mode.
- Run: `podman-compose up -d`


Verify start up by surfing into Opensearch Dashboards in a browser: localhost:5601

### Stopping
To stop and remove containers
- Run: `podman-compose down`



### Troubleshoot Podman for desktop on Windows:
If opensearch fails to start in podman for desktop on Windows due to 'vm.max_map_count [65530] is too low':

- In Powershell as admin:
```
podman-machine ssh
sudo sysctl -w vm.max_map_count=262144
```

## Load taxonomy index into opensearch:
Index for taxonomy is created with [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers)

An alternative way is described in `docs/copy_indices_using_elasticdump.md`



# Set up repo in IntelliJ or PyCharm:
- Install Python 3.11.4+ if not already done. Keep track of installation dir. https://www.python.org/downloads/
- Install poetry on computer based on this guide: https://www.jetbrains.com/help/idea/poetry.html#baba6780
  - On windows, open power shell:
  - Install: 
    ```
    (Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py
    ```
  - Set env-variable (i.e where windows user is the logged in in user. Don't forget to change value for [windows user] before running):
    ```
    $Env:Path += ";C:\Users\[windows user]\AppData\Roaming\Python\Scripts"; setx PATH "$Env:Path"
    ```
- Create a Poetry environment in IntelliJ or PyCharm:
  - Ctrl+Alt+Shift+S -> SDK:s -> Add Python SDK -> Poetry Environment:
  - Base Interpreter = path to Python 3.11.4+ installation.
  - Poetry Executable = path to Poetry installation, same as env-variable for poetry.
  - Choose Poetry environment in IntelliJ or PyCharm:
    - Ctrl+Alt+Shift+S -> Project -> SDK: Choose your newly created environment, "Poetry (joblinks-search-api)

  
## Import from file:
A file is produced nightly by the JobAd Links pipeline, it can be imported:
- Download a recent file from: https://data.jobtechdev.se/annonser/jobtechlinks/index.html
- Save (and unpack if gzip) file to a recognizable folder and file name, e.g. [joblinks-importer-repo]\resources\output_2023-01-01.json
- Set environment variables (found in `settings.py`):
  - `JOBLINKS_READ_FROM_FILE=true`
  - `JOBLINKS_FILE_PATTERN` must match the unpacked file
  - `JOBLINKS_FOLDER` if the file is not in the default directory.
  
Alternatively, the file can be imported from `stdin` in PowerShell:
Open PowerShell (this does not work in CMD) and go to the root of this repo:
- activate the virtual environment: .\.venv\Scripts\activate.ps1 (must be .ps1 to work in PowerShell)
- Run `cat .\resources\<joblinks-file>.json|python .\main.py`

## Set up run configuration in IntelliJ or Pycharm:
- Go to menu "Run" and choose "Edit configurations"
- Add new config -> Python
  - Script Path:[path to repo]\main.py
  - Env. variables: JOBLINKS_READ_FROM_FILE=true
  Also check if any other environment variables need to be changed from default as configured in settings.py.
  - Working directory:[path to repo]
  - Check "Add content roots to PYTHONPATH" + Check "Add source roots to PYTHONPATH"
  
- Click "run" to parse the ads file and import to opensearch.

## Running tests
Make sure Pytest are preselected in IntelliJ:
- Ctrl Alt S -> Python integrated tools -> Testing : pytest
`pytest tests`
For the tests in the JobAd Links API project, the ads from 2022-03-08 are used and must be imported


## Building an image 
from the root directory: `podman build -t joblinks-importer .`

## Run using ads from stdin (i.e. with an hard coded test):
docker build -t import-to-elastic .
echo '{"id": "0807a0a6f4b2f92400f17d855acc918d", "firstSeen": "2020-11-08T08:57:39", "originalJobPosting": {"hiringOrganization": {"name": "Försäkringskassan", "address": "Försäkringskassan\nFörsäkringskassan\n10351 Stockholm", "@context": "http://schema.org/", "@type": "Organization", "url": "http://www.forsakringskassan.se/omfk/jobba_hos_oss"}, "@context": "http://schema.org/", "jobLocation": {"address": "Karlshamn", "@context": "http://schema.org/", "@type": "Place"}, "@type": "JobPosting", "url": "https://arbetsformedlingen.se/platsbanken/annonser/24340836", "employmentType": "Tillsvidare- eller tidsbegränsad anställning", "relevantOccupation": {"@type": "Occupation", "occupationalCategory": {"@type": "CategoryCode", "codeValue": "5570", "@context": "http://schema.org/"}, "@context": "http://schema.org/", "name": "Försäkringshandläggare, försäkringskassa"}, "experienceRequirements": "Försäkringshandläggare, försäkringskassa - Erfarenhet efterfrågas", "datePosted": "2020-11-02", "identifier": "24340836", "validThrough": "2020-11-17", "totalJobOpenings": 10, "title": "Försäkringshandläggare inom bostadsbidrag i Karlshamn", "scraper": "arbetsformedlingen.se"}, "detected_language": "sv", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Försäkringshandläggare", "term": "försäkringshandläggare", "term_misspelled": false, "prediction": 0.719095826}, {"concept_label": "Försäkringshandläggare", "term": "försäkringshandläggare", "term_misspelled": false, "prediction": 0.897734612}, {"concept_label": "Försäkringshandläggare", "term": "försäkringshandläggare", "term_misspelled": false, "prediction": 0.867224842}, {"concept_label": "Förvaltare", "term": "förvaltare", "competenciesterm_misspelled": false, "prediction": 0.066311121}, {"concept_label": "Hr-specialist", "term": "hr-specialist", "term_misspelled": false, "prediction": 0.644071817}], "competencies": [{"concept_label": "Myndigheter", "term": "myndigheter", "term_misspelled": false, "prediction": 0.085342526}, {"concept_label": "Svenska", "term": "svenska", "term_misspelled": false, "prediction": 0.170793355}, {"concept_label": "Regelverk", "term": "regelverk", "term_misspelled": false, "prediction": 0.782099694}, {"concept_label": "Skatt", "term": "skatt", "term_misspelled": false, "prediction": 0.237976193}, {"concept_label": "Myndigheter", "term": "myndigheter", "term_misspelled": false, "prediction": 0.675559103}, {"concept_label": "Bank", "term": "banker", "term_misspelled": false, "prediction": 0.698218346}, {"concept_label": "Datorvana", "term": "datorvana", "term_misspelled": false, "prediction": 0.860973}, {"concept_label": "Målstyrning", "term": "målstyrning", "term_misspelled": false, "prediction": 0.9826355875}, {"concept_label": "Resultatstyrning", "term": "resultatstyrning", "term_misspelled": false, "prediction": 0.9805099666}, {"concept_label": "Arbetsmiljö", "term": "arbetsmiljö", "term_misspelled": false, "prediction": 0.849249452}, {"concept_label": "Verktyg", "term": "verktyg", "term_misspelled": false, "prediction": 0.9856696427}, {"concept_label": "Personkontroll", "term": "personkontroll", "term_misspelled": false, "prediction": 0.365949869}, {"concept_label": "Rekrytering", "term": "rekrytering", "term_misspelled": false, "prediction": 0.054606915}, {"concept_label": "Tester", "term": "tester", "term_misspelled": false, "prediction": 0.103765607}], "traits": [{"concept_label": "Serviceinriktad", "term": "serviceorienterad", "term_misspelled": false, "prediction": 0.888511777}, {"concept_label": "Stabil", "term": "stabil", "term_misspelled": false, "prediction": 0.935000896}, {"concept_label": "Kommunikativ", "term": "kommunikativ förmåga", "term_misspelled": false, "prediction": 0.865231812}, {"concept_label": "Kvalitetsmedveten", "term": "kvalitetsmedveten", "term_misspelled": false, "prediction": 0.959069312}, {"concept_label": "Pedagogisk", "term": "pedagogisk förmåga", "term_misspelled": false, "prediction": 0.0245114863}, {"concept_label": "Flexibel", "term": "flexibel", "term_misspelled": false, "prediction": 0.94398421}], "geos": [{"concept_label": "Karlshamn", "term": "karlshamn", "term_misspelled": false, "prediction": 0.410829365}]}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "Försäkringshandläggare", "term": "försäkringshandläggare", "term_misspelled": false}], "competencies": [{"concept_label": "Myndigheter", "term": "myndigheter", "term_misspelled": false}, {"concept_label": "Bank", "term": "banker", "term_misspelled": false}, {"concept_label": "Regelverk", "term": "regelverk", "term_misspelled": false}, {"concept_label": "Arbetsmiljö", "term": "arbetsmiljö", "term_misspelled": false}, {"concept_label": "Datorvana", "term": "datorvana", "term_misspelled": false}, {"concept_label": "Resultatstyrning", "term": "resultatstyrning", "term_misspelled": false}, {"concept_label": "Målstyrning", "term": "målstyrning", "term_misspelled": false}, {"concept_label": "Verktyg", "term": "verktyg", "term_misspelled": false}], "traits": [{"concept_label": "Kommunikativ", "term": "kommunikativ förmåga", "term_misspelled": false}, {"concept_label": "Serviceinriktad", "term": "serviceorienterad", "term_misspelled": false}, {"concept_label": "Stabil", "term": "stabil", "term_misspelled": false}, {"concept_label": "Flexibel", "term": "flexibel", "term_misspelled": false}, {"concept_label": "Kvalitetsmedveten", "term": "kvalitetsmedveten", "term_misspelled": false}], "geos": [{"concept_label": "Karlshamn", "term": "karlshamn", "term_misspelled": false}]}}}, "yrke": [{"concept_label": "Konsultchef", "term": "konsultchef"}, {"concept_label": "Läkare", "term": "läkare"}, {"concept_label": "Specialistläkare", "term": "specialistläkare"}], "ort": [], "hashsum": "AAzDrcOPAMKIX8KtAMOiI8KTADctwpE=ARgaw5M=Aih0wog=AsOoNEA=AcKawp/Dsg+5f92cc35412861fed98702d98a1a9e40", "municipality_concept_id": "", "region_concept_id": "", "ssyk_lvl4": 3353, "isValid":true, "brief_description": "Försäkringskassan är en av Sveriges största myndigheter. Vi spelar en stor roll för den enskilde individen och för välfärden i det svenska samhället."},
{"id": "3b8ea0ce0967a59cb2dd5478d386c021", "firstSeen": "2020-11-08T08:57:39", "originalJobPosting": {"hiringOrganization": {"name": "PayPal SE, Ireland, Filial Sweden", "address": "PayPal SE, Ireland, Filial Sweden\n\n ", "@context": "http://schema.org/", "@type": "Organization", "url": ""}, "@context": "http://schema.org/", "jobLocation": {"@type": "Place", "@context": "http://schema.org/", "address": "Stockholm"}, "@type": "JobPosting", "url": "https://arbetsformedlingen.se/platsbanken/annonser/24299303", "employmentType": "Tillsvidare- eller tidsbegränsad anställning", "relevantOccupation": {"occupationalCategory": {"codeValue": "5703", "@context": "http://schema.org/", "@type": "CategoryCode"}, "@type": "Occupation", "name": "Statistiker", "@context": "http://schema.org/"}, "experienceRequirements": "Statistiker - 1-2 års erfarenhet", "datePosted": "2020-10-12", "identifier": "24299303", "validThrough": "2020-11-11", "totalJobOpenings": 1, "title": "Product Analyst", "scraper": "arbetsformedlingen.se"}, "detected_language": "en", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Major", "term": "major", "term_misspelled": false, "prediction": 0.079498231}, {"concept_label": "Diskrimineringsombudsman", "term": "do", "term_misspelled": false, "prediction": 0.62818861}], "competencies": [{"concept_label": "Mission", "term": "mission", "term_misspelled": false, "prediction": 0.203518867}, {"concept_label": "Tens", "term": "tens", "term_misspelled": false, "prediction": 0.410510898}, {"concept_label": "Can", "term": "can", "term_misspelled": false, "prediction": 0.553154796}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.586963743}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.861661732}, {"concept_label": "Can", "term": "can", "term_misspelled": false, "prediction": 0.9651736915}, {"concept_label": "Product managers", "term": "product managers", "term_misspelled": false, "prediction": 0.569024056}, {"concept_label": "User experience", "term": "user experience", "term_misspelled": false, "prediction": 0.822427481}, {"concept_label": "Feature", "term": "feature", "term_misspelled": false, "prediction": 0.692188501}, {"concept_label": "Feature", "term": "feature", "term_misspelled": false, "prediction": 0.375546873}, {"concept_label": "Feature", "term": "feature", "term_misspelled": false, "prediction": 0.754650205}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.740049303}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.645409882}, {"concept_label": "Not", "term": "not", "term_misspelled": false, "prediction": 0.375591636}, {"concept_label": "Cvs", "term": "cvs", "term_misspelled": false, "prediction": 0.323218405}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.59515065}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.616934329}, {"concept_label": "Sql", "term": "sql", "term_misspelled": false, "prediction": 0.570579946}, {"concept_label": "Script", "term": "scripting", "term_misspelled": false, "prediction": 0.478263557}, {"concept_label": "Python", "term": "python", "term_misspelled": false, "prediction": 0.560503513}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.535663426}, {"concept_label": "Business intelligence", "term": "business intelligence", "term_misspelled": false, "prediction": 0.727286041}, {"concept_label": "Tableau", "term": "tableau", "term_misspelled": false, "prediction": 0.628321111}, {"concept_label": "Design", "term": "design", "term_misspelled": false, "prediction": 0.823562562}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.822893858}, {"concept_label": "Data", "term": "data", "term_misspelled": false, "prediction": 0.746674806}, {"concept_label": "Can", "term": "can", "term_misspelled": false, "prediction": 0.23060143}, {"concept_label": "Microsoft access", "term": "access", "term_misspelled": false, "prediction": 0.243604898}, {"concept_label": "Most", "term": "most", "term_misspelled": false, "prediction": 0.121835291}], "traits": [], "geos": []}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "UNKNOWN", "term": "", "term_misspelled": false}], "competencies": [{"concept_label": "Data", "term": "data", "term_misspelled": false}, {"concept_label": "Can", "term": "can", "term_misspelled": false}, {"concept_label": "Python", "term": "python", "term_misspelled": false}, {"concept_label": "Product managers", "term": "product managers", "term_misspelled": false}, {"concept_label": "Sql", "term": "sql", "term_misspelled": false}, {"concept_label": "Tableau", "term": "tableau", "term_misspelled": false}, {"concept_label": "Feature", "term": "feature", "term_misspelled": false}, {"concept_label": "Business intelligence", "term": "business intelligence", "term_misspelled": false}, {"concept_label": "User experience", "term": "user experience", "term_misspelled": false}, {"concept_label": "Design", "term": "design", "term_misspelled": false}], "traits": [], "geos": []}}}, "yrke": [{"concept_label": "Konsultchef", "term": "konsultchef"}, {"concept_label": "Läkare", "term": "läkare"}, {"concept_label": "Specialistläkare", "term": "specialistläkare"}], "ort": [], "hashsum": "AhjCuMKBAMO8JMKlAcOQw6jCvwAT8Lw74=AMKvw77DrAAMKOw5bCrgAAwgwrU=ACjDmWM=+21befed78a0aafd14edca8a053add16e", "municipality_concept_id": "", "region_concept_id": "", "ssyk_lvl4": 2512, "isValid":true, "brief_description": "In 2018, iZettle became part of the PayPal family. iZettle is on a mission to help small businesses succeed in a world of giants."},
{"firstSeen": "2020-11-08T08:57:39", "id": "78b959b57eeeb189a5f528162a845f4a", "originalJobPosting": {"hiringOrganization": {"@type": "Organization", "url": "http://www.lnu.se", "name": "Linnéuniversitetet", "@context": "http://schema.org/", "address": "Linnéuniversitetet\nLinnéuniversitetet\n35195 Växjö"}, "@context": "http://schema.org/", "relevantOccupation": {"name": "Åklagare", "@context": "http://schema.org/", "occupationalCategory": {"@context": "http://schema.org/", "codeValue": "6031", "@type": "CategoryCode"}, "@type": "Occupation"}, "experienceRequirements": "Åklagare - Erfarenhet efterfrågas", "datePosted": "2020-11-02", "jobLocation": {"@type": "Place", "@context": "http://schema.org/", "address": "Växjö"}, "url": "https://arbetsformedlingen.se/platsbanken/annonser/24341230", "@type": "JobPosting", "employmentType": "Tillsvidare- eller tidsbegränsad anställning", "title": "Expert i juridik", "identifier": "24341230", "validThrough": "2020-12-01", "totalJobOpenings": 1, "scraper": "arbetsformedlingen.se"}, "detected_language": "sv", "text_enrichments_results": {"enriched_result": {"enriched_candidates": {"occupations": [{"concept_label": "Prefekt", "term": "prefekt", "term_misspelled": false, "prediction": 0.2924}, {"concept_label": "Hr-partner", "term": "hr-partner", "term_misspelled": false, "prediction": 0.581684351}, {"concept_label": "Försäljare", "term": "försäljare", "term_misspelled": false, "prediction": 0.024174929}], "competencies": [{"concept_label": "Juridik", "term": "juridik", "term_misspelled": false, "prediction": 0.428245246}, {"concept_label": "Samhällsutveckling", "term": "samhällsutveckling", "term_misspelled": false, "prediction": 0.018719852}, {"concept_label": "Va", "term": "va", "term_misspelled": false, "prediction": 0.203439951}, {"concept_label": "Grundutbildning", "term": "grundutbildning", "term_misspelled": false, "prediction": 0.125307739}, {"concept_label": "Forskning", "term": "forskning", "term_misspelled": false, "prediction": 0.254000425}, {"concept_label": "Juridik", "term": "juridik", "term_misspelled": false, "prediction": 0.764074415}, {"concept_label": "Undervisning", "term": "undervisning", "term_misspelled": false, "prediction": 0.694185853}, {"concept_label": "Kursutveckling", "term": "kursutveckling", "term_misspelled": false, "prediction": 0.821139127}, {"concept_label": "Straffrätt", "term": "straffrätt", "term_misspelled": false, "prediction": 0.50796324}, {"concept_label": "Processrätt", "term": "processrätt", "term_misspelled": false, "prediction": 0.530211508}, {"concept_label": "Samverkan", "term": "samverkan", "term_misspelled": false, "prediction": 0.195767879}, {"concept_label": "Samverkan", "term": "samverkan", "term_misspelled": false, "prediction": 0.264085889}, {"concept_label": "Undervisning", "term": "undervisning", "term_misspelled": false, "prediction": 0.266888618}, {"concept_label": "Forskning", "term": "forskning", "term_misspelled": false, "prediction": 0.281400919}, {"concept_label": "Juristexamen", "term": "juristexamen", "term_misspelled": false, "prediction": 0.85175404}, {"concept_label": "Ga", "term": "ga", "term_misspelled": false, "prediction": 0.848398566}, {"concept_label": "Myndigheter", "term": "myndigheter", "term_misspelled": false, "prediction": 0.891907334}, {"concept_label": "Organisationer", "term": "organisationer", "term_misspelled": false, "prediction": 0.851567328}, {"concept_label": "Va", "term": "va", "term_misspelled": false, "prediction": 0.484206378}, {"concept_label": "Va", "term": "va", "term_misspelled": false, "prediction": 0.173718572}, {"concept_label": "Telefonsamtal", "term": "telefonsamtal", "term_misspelled": false, "prediction": 0.029812217}], "traits": [{"concept_label": "Pedagogisk", "term": "pedagogisk förmåga", "term_misspelled": false, "prediction": 0.916065454}], "geos": [{"concept_label": "Småland", "term": "småland", "term_misspelled": false, "prediction": 0.0967582464}]}}, "enrichedbinary_result": {"enriched_candidates": {"occupations": [{"concept_label": "UNKNOWN", "term": "", "term_misspelled": false}], "competencies": [{"concept_label": "Straffrätt", "term": "straffrätt", "term_misspelled": false}, {"concept_label": "Processrätt", "term": "processrätt", "term_misspelled": false}, {"concept_label": "Undervisning", "term": "undervisning", "term_misspelled": false}, {"concept_label": "Juridik", "term": "juridik", "term_misspelled": false}, {"concept_label": "Kursutveckling", "term": "kursutveckling", "term_misspelled": false}, {"concept_label": "Ga", "term": "ga", "term_misspelled": false}, {"concept_label": "Organisationer", "term": "organisationer", "term_misspelled": false}, {"concept_label": "Juristexamen", "term": "juristexamen", "term_misspelled": false}, {"concept_label": "Myndigheter", "term": "myndigheter", "term_misspelled": false}], "traits": [{"concept_label": "Pedagogisk", "term": "pedagogisk förmåga", "term_misspelled": false}], "geos": []}}}, "yrke": [{"concept_label": "Konsultchef", "term": "konsultchef"}, {"concept_label": "Läkare", "term": "läkare"}, {"concept_label": "Specialistläkare", "term": "specialistläkare"}], "ort": [], "hashsum": "AAzDrcOPAF7Du14=AsK/QC8=AWrChsKPAMO0wpfDswADPDmEM=ABpnw7g=Am5pYQ+e9940cf174c76f2fb301259230574b0c", "municipality_concept_id": "", "region_concept_id": "", "ssyk_lvl4": 2319, "isValid":true, "brief_description": "Välkommen till Linnéuniversitetet! Människor växer här."}' | docker run -i import-to-elastic

To run this locally with opensearch on localhost, use:
podman run --network=host -i joblinks-importer --secrets env.sh


# Troubleshooting

If the tests, for some reason, is not passed.
If the populating of joblinks returns with taxonomy-warnings
========================================================================

- Check if the following query returns the correct number of ads (1), in Kibana
  GET /taxonomy/_search
  {"query": {"bool": {"must": [{"term": {"legacy_ams_taxonomy_id": {"value": 2313}}}, {"term": {"type": {"value": "occupation-group"}}}]}}}

If not:
- delete the indices(!) joblinks and taxonomy
- Copy the /_settings from taxonomy.develop and add them when creating the local index in Kibana by the PUT api
- Copy the /_mappings from taxonomy.develop and add it to the local index in Kibana by the PUT api
- Do a reindex of the taxonomy.develop index
- The query above should now return the correct number of ads (1)
- Populate the joblinks index

The tests should now pass, and the population of the joblinks index should also go through with out to many warnings.
