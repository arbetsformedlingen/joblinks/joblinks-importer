import sys
import itertools
import time
from loguru_wrapper import log
import repository.mappings
from repository.converter import convert_batch
from repository import opensearch
from repository.in_data_handling import get_ads, check_number_of_valid_ads
import settings
from opensearchpy.exceptions import NotFoundError

def _grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])


def _convert_and_save_to_opensearch(ads, es_index):
    len_ads = len(ads)
    len_converted = 0
    log.info(f'Converting ad details with len: {len_ads}')
    nr_of_items_per_batch = min(settings.BATCH_SIZE, len_ads)
    ad_batches = _grouper(nr_of_items_per_batch, ads)
    for ad_batch in ad_batches:
        converted_ads_batch = convert_batch(ad_batch)
        opensearch.bulk_index(converted_ads_batch, es_index)
        log.info(f"Saved ads: {len(converted_ads_batch)} to Opensearch index: {es_index}")
        len_converted += len(converted_ads_batch)

    return len_converted


def _calculate_time(start_time, len_ads):
    elapsed_time = time.time() - start_time
    m, s = divmod(elapsed_time, 60)
    log.info(f"Imported {len_ads} docs in: {int(m)} minutes {int(s)} seconds.")


def _start():
    log.info(f'Starting with version: {settings.VERSION}')

    start_time = time.time()

    ads, file_timestamp = get_ads()
    ads_number_current_index = int(opensearch.document_count(settings.ES_JOBLINKS_ALIAS))
    change_alias = check_number_of_valid_ads(ads, ads_number_current_index, file_timestamp)
    es_index = opensearch.setup_index(file_timestamp, settings.ES_JOBLINKS_ALIAS, repository.mappings.joblinks_mappings)
    log.info(f"Starting ad import into index: {es_index}")
    len_ads = _convert_and_save_to_opensearch(ads, es_index)
    opensearch.verify_import(es_index, len_ads)
    if change_alias:
        _change_alias([es_index], settings.ES_JOBLINKS_ALIAS)
        start_exit_code = 0
    else:
        start_exit_code = 1
        log.error(f"Alias: {settings.ES_JOBLINKS_ALIAS} NOT changed to: {es_index}. Exit with code: {start_exit_code}!")
    _calculate_time(start_time, len_ads)
    return start_exit_code


def _change_alias(idxnames, aliasname):
    log.info(f"Setting alias: {aliasname} to point to: {idxnames}")
    try:
        if opensearch.alias_exists(aliasname):
            oldindices = list(opensearch.get_alias(aliasname).keys())
            opensearch.update_alias(idxnames, oldindices, aliasname)
        else:
            opensearch.add_indices_to_alias(idxnames, aliasname)
    except NotFoundError as e:
        log.error(f"Can't create alias: {aliasname}. Indices not found: {idxnames}.")
        raise e


if __name__ == '__main__':
    exit_code = _start()
    sys.exit(exit_code)
