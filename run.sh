#!/usr/bin/env bash
set -eEu -o pipefail

# This script's directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

DOCKERFLAGS=${DOCKERFLAGS:-}


# Run docker from utility (first change CWD!)
cd "$script_dir" && ../../bin/container_ops.sh $DOCKERFLAGS -- $@
